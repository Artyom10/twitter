package com.example.twitter.storage

interface Crud<E> {

    fun create(message: String)

    fun read(callback: (List<E>) -> Unit): List<E>

    fun update(callback: (List<E>) -> Unit)

    fun delete(index: Int)

}