package com.example.twitter.storage

import com.example.twitter.model.TweetItem
import com.example.twitter.util.JsonPlaceHolderApiImpl
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object TemporaryDataStorage : Crud<TweetItem>  {

    private var twitterCollection: MutableList<TweetItem> = mutableListOf()

    override fun create(message: String) {
        JsonPlaceHolderApiImpl.twitterAPI
            .postTimeLine(message)
            .enqueue(object : Callback<TweetItem> {
                override fun onFailure(call: Call<TweetItem>, t: Throwable) {
                    println("Failed to execute request")
                }

                override fun onResponse(call: Call<TweetItem>, response: Response<TweetItem>) {
                    val tweet = response.body()
                    if(tweet != null) {
                        twitterCollection.add(0, tweet)
                    }
                    println("Call result: $tweet")
                }

            })
        update {}
    }

    override fun read(callback: (List<TweetItem>) -> Unit) : List<TweetItem> {
        update(callback)
        return twitterCollection
    }

    override fun update(callback: (List<TweetItem>) -> Unit) {
        JsonPlaceHolderApiImpl.twitterAPI
            .getTimeLine()
            .enqueue(object : Callback<List<TweetItem>> {
                override fun onFailure(call: Call<List<TweetItem>>, t: Throwable) {
                    println("Failed to execute request")
                }

                override fun onResponse(call: Call<List<TweetItem>>, response: Response<List<TweetItem>>) {
                    val tweet = response.body()
                    if (tweet != null) {
                        twitterCollection = tweet.toMutableList()
                        callback(twitterCollection)
                    }
                    println(tweet)
                    println("Call result: ${twitterCollection.joinToString(separator = "\n")}")
                }
            })
    }

    override fun delete(index: Int) {
       if(index < twitterCollection.size) {
           twitterCollection.removeAt(index)
       }
    }
}