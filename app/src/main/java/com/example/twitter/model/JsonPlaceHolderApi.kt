package com.example.twitter.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface JsonPlaceHolderApi {

    @GET("statuses/home_timeline.json")
    fun getTimeLine(): Call<List<TweetItem>>

    @POST("statuses/update.json")
    fun postTimeLine(@Query("status") message: String): Call<TweetItem>
}