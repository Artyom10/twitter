package com.example.twitter.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TweetItem(
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("retweet_count")
    val retweetCount: Int? = null,
    @SerializedName("favorite_count")
    val favoriteCount: Int? = null,
    @SerializedName("user")
    val user: User
) : Parcelable