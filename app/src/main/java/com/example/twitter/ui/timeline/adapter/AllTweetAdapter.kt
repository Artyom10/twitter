package com.example.twitter.ui.timeline.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.twitter.R
import com.example.twitter.model.TweetItem
import com.example.twitter.util.DateParsing
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_tweet.*

class AllTweetAdapter(private val tweetList : List<TweetItem>) :
        RecyclerView.Adapter<AllTweetAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(parent.context)
        val view: View = when(viewType) {
            TWEET ->
                inflater.inflate(R.layout.item_tweet, parent, false)
            PROMOTION ->
                inflater.inflate(R.layout.item_tweet_promotion, parent, false)
            else -> (throw IllegalArgumentException("Error"))
        }
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
       return tweetList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tweet = tweetList[position]
        holder.bind(tweet)
    }

    class ViewHolder(override val containerView: View) :
            RecyclerView.ViewHolder(containerView), LayoutContainer {

        private val requestManager = Glide.with(containerView.context)

        fun bind(tweet: TweetItem) {
            requestManager.clear(person)

            requestManager.load(tweet.user.profileImageUrlHttps)
                .circleCrop()
                .into(person)

            name.text = tweet.user.name
            time.text = DateParsing.gettingDate(tweet.createdAt)
            idName.text = tweet.user.screenName
            status.text = tweet.text
            comments.text = tweet.retweetCount.toString()
            likes.text = tweet.favoriteCount.toString()
        }
    }

    companion object {
        const val TWEET = 0
        const val PROMOTION = 1
    }
}