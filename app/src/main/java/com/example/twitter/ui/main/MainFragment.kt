package com.example.twitter.ui.main

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.twitter.R
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addTweetButton.setOnClickListener {
            it.findNavController().navigate(R.id.action_mainFragment_to_createTweetFragment)
        }
        showTweetsButton.setOnClickListener {
            it.findNavController().navigate(R.id.action_mainFragment_to_allTweetFragment)
        }
    }
}