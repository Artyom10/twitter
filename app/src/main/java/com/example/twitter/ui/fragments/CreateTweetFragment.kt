package com.example.twitter.ui.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import com.example.twitter.R
import com.example.twitter.storage.TemporaryDataStorage
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_create.*

class CreateTweetFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_create, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sendTweetButton.setOnClickListener {
            savingDataToTemporaryStorage(view)
        }
    }

    private fun savingDataToTemporaryStorage(view:View) {
        val message = editTextMessage.text.toString()
        TemporaryDataStorage.create(message)
        Snackbar.make(view, "Tweet send", Snackbar.LENGTH_SHORT).show()
    }
}