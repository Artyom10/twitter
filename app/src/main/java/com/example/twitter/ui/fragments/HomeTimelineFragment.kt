package com.example.twitter.ui.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.twitter.R
import com.example.twitter.model.TweetItem
import com.example.twitter.storage.TemporaryDataStorage
import com.example.twitter.ui.timeline.adapter.AllTweetAdapter
import kotlinx.android.synthetic.main.fragment_all_tweet.*

class HomeTimelineFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(R.layout.fragment_all_tweet, container, false)
    }

    fun set(collection: List<TweetItem>) {
        tweetRecyclerView.adapter = AllTweetAdapter(collection)
        tweetRecyclerView.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        TemporaryDataStorage.read(::set)
    }
}